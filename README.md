# SublimeConfig

My personal configuration of sublime

# Installed package

- Theme Adaptative
- Color-Scheme Mariana
- Aligment: https://packagecontrol.io/packages/Alignment
- All autocomplete: https://packagecontrol.io/packages/All%20Autcomplete
- BracketHighlighter: https://packagecontrol.io/packages/BracketHighlighter
- DocBlockr: https://packagecontrol.io/packages/DocBlockr
- SublimeCodeIntel: https://packagecontrol.io/packages/SublimeCodeIntel
- SublimeLinter https://packagecontrol.io/packages/SublimeLinter
- SublimeLinter-php https://packagecontrol.io/packages/SublimeLinter-php

I Hope this can help you :)